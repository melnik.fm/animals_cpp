﻿#include <iostream>
#include <ctime>

class Animal
{
public:
    virtual void Voice() = 0;

};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow" << std::endl;
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo" << std::endl;
    }
};


int main()
{

    srand(time(NULL));
    Cat cCat;
    Cow cCow;
    Dog cDog;

    const int Limit = 20;
    Animal* Animal[Limit];

    for (int i = 0; i < Limit; i++)
    {
        int x = rand() % 3;
        switch (x)
        {
        case 0: 
            Animal[i] = & cCat;
            break;
        case 1: 
            Animal[i] = & cCow;
            break;
        case 2: 
            Animal[i] = & cDog;
            break;
        }
    }

    for (int i = 0; i < Limit; i++)
    {
        Animal[i]->Voice();
    }
 





}

